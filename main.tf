resource "google_compute_firewall" "fw_rules" {
  for_each = { for rule in var.ingress_fw_rules : rule.name => rule }

  project = var.gcp_network_project_id
  network = var.gcp_network_name

  name = "${module.name.short}-fw-allow-${each.value.name}-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = flatten([["443"], each.value.http_enabled ? ["80"] : []])
  }

  source_ranges = each.value.ips_cidrs
  target_tags   = var.gke_node_pool_network_tags
  priority      = 30

  dynamic "log_config" {
    for_each = each.value.logging_enabled ? [1] : []
    content {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }
}