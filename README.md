<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [google_compute_firewall.fw_rules](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_network_name"></a> [gcp\_network\_name](#input\_gcp\_network\_name) | n/a | `string` | n/a | yes |
| <a name="input_gcp_network_project_id"></a> [gcp\_network\_project\_id](#input\_gcp\_network\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gke_node_pool_network_tags"></a> [gke\_node\_pool\_network\_tags](#input\_gke\_node\_pool\_network\_tags) | n/a | `list(string)` | n/a | yes |
| <a name="input_ingress_class_name"></a> [ingress\_class\_name](#input\_ingress\_class\_name) | n/a | `string` | n/a | yes |
| <a name="input_ingress_fw_app_name"></a> [ingress\_fw\_app\_name](#input\_ingress\_fw\_app\_name) | n/a | `string` | n/a | yes |
| <a name="input_ingress_fw_rules"></a> [ingress\_fw\_rules](#input\_ingress\_fw\_rules) | n/a | <pre>list(object({<br>    name            = string<br>    ips_cidrs       = list(string)<br>    logging_enabled = bool<br>    http_enabled    = bool<br>  }))</pre> | n/a | yes |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->