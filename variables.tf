variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}

variable "gcp_network_project_id" {
  type = string
}
variable "gcp_network_name" {
  type = string
}

variable "ingress_node_pool_network_tags" {
  type = list(string)
}
variable "ingress_class_name" {
  type = string
}
variable "ingress_fw_rules" {
  type = list(object({
    name            = string
    ips_cidrs       = list(string)
    logging_enabled = bool
    http_enabled    = bool
  }))
}
variable "ingress_fw_app_name" {
  type = string
}
